'use strict';
module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    username: { type: DataTypes.STRING, unique: true },
    password: DataTypes.STRING,
    moderator: { type: DataTypes.BOOLEAN, defaultValue: false },
    admin: { type: DataTypes.BOOLEAN, defaultValue: false },
    joined: { type: DataTypes.STRING }
  }, {
    timestamps: false,
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        User.hasMany(models.Post);
      }
    }
  });
  return User;
};
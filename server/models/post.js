'use strict';
module.exports = function(sequelize, DataTypes) {

  var Post = sequelize.define('Post', {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    title: DataTypes.STRING,
    body: DataTypes.TEXT,
    upvotes: { type: DataTypes.INTEGER, defaultValue: 0 },
    date_published: DataTypes.STRING,
    date_updated: DataTypes.STRING
  }, {
    timestamps: false,
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Post.belongsTo(models.User);
      }
    }
  });
  return Post;
};
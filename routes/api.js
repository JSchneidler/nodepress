var express = require('express');
var router = express.Router();

router.use(function(req, res, next) {
	console.log("API request made");
	next();
})

router.get('/', function(req, res) {
	res.send("Welcome to the API.");
});

module.exports = router;
var express = require('express');
var app = express();
var router = express.Router();
var jwt_middleware = require('./middleware/jwt');
var _ = require('lodash');

var models = require('../server/models/index');
var Post = models.Post;
var User = models.User;

router.get('/posts', function(req, res) {
	Post.findAll({}).then(function(posts) {
		return res.json({
			success: true,
			data: formatPosts(posts)
		});
	}).error(function(err) {
		console.log(err);
		return res.json({
			success: false,	
			message: "Server error"
		});
	});
});

router.post('/posts', jwt_middleware, function(req, res) {
	var post = req.body;
	if (post.title && post.body) {
		Post.create({
			title: post.title,
			body: post.body,
			date_published: Date.now().toString(),
			date_updated: Date.now().toString()
		}).then(function(post) {
			User.findOne({where: { id: post.user_id }}).then(function(user) {
				post.setUser(user).then(function(){
					return res.json({
						success: true,
						data: formatPost(post)
					});
				}).error(function(err) {
					console.log(err);
					return res.json({
						success: false,
						message: "Server error"
					});
				});
			}).error(function(err) {
				console.log(err);
				return res.json({
					success: false,
					message: "Server error"
				});
			});
		}).error(function(err) {
			console.log(err);
			return res.json({
				success: false,
				message: "Server error"
			});
		});
	} else {
		return res.json({
			success: false,
			message: "Missing post title or body"
		});
	}
});

router.get('/posts/:id', function(req, res) {
	var id = req.params.id;
	Post.findOne({where: { id: id }}).then(function(post) {
		return res.json({
			success: true,
			data: formatPost(post)
		});
	}).error(function(err){
		console.log(err);
		return res.json({
			success: false,
			message: "Server error"
		});
	});
});

router.get('/posts/:id/upvote', function(req, res) {
	var id = req.params.id;
	console.log(id);
	Post.findOne({where: { id: id }}).then(function(post) {
		console.log(post);
		post.update({ upvotes: ++post.upvotes }).then(function() {
			return res.json({
				success: true,
				data: 'Post upvoted'
			});
		}).error(function(err) {
			console.log(err);
			return res.json({
				success: false,
				message: "Server error updating post"
			});
		});
	}).error(function(err) {
		console.log(err);
		return res.json({
			success: false,
			message: "Server error"
		});
	});
});

router.put('/posts/:id', jwt_middleware, function(req, res) {
	var id = req.params.id;
	var body = req.body;
	body.date_updated = Date.now().toString();
	if (body) {
		Post.findOne({where: { id: id }}).then(function(post) {
			post.update(body).then(function() {
				return res.json({
					success: true,
					data: "Post update successfully"
				})
			}).error(function(err) {
				console.log(err);
				return res.json({
					success: false,
					message: "Server error"
				});
			});
		}).error(function(err) {
			console.log(err);
			return res.json({
				success: false,
				message: "Server error"
			});
		});
	} else {
		return res.json({
			success: false,
			message: "Missing updates"
		});
	}
});

router.delete('/posts/:id', jwt_middleware, function(req, res) {
	var id = req.params.id;
	Post.findOne({where: { id: id }}).then(function(post) {
		post.destroy().then(function() {
			return res.json({
				success: true,
				data: "Post deleted successfully"
			});
		}).error(function(err) {
			console.log(err);
			return res.json({
				success: false,
				message: "Server error"
			});
		});
	}).error(function(err) {
		console.log(err);
		return res.json({
			success: false,
			message: "Server error"
		});
	});
});

function formatPost(post) {
	console.log(post);
	return _.omit(JSON.parse(JSON.stringify(post)), ['__v', 'UserId']);
}

function formatPosts(posts) {
	var formatted = [];
	for (var i = 0; i < posts.length; i++) {
		formatted.push(formatPost(posts[i]));
	}
	return formatted;
}

module.exports = router;
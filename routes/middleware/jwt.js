var jwt = require('jsonwebtoken');
var jwt_config = require('../../config/jwt');

module.exports = function(req, res, next) {
	if (req.headers.authorization) {
		var token = req.headers.authorization.substring(7); // Remove 'Bearer ' from token
		jwt.verify(token, jwt_config.secret, function(err, decoded) {
			if (err) {
				console.log(err);
				return res.json({
					success: false,
					message: "Server error"
				});
			}
			// Token valid, continue route.
			next();
		});
	} else {
		return res.json({
			success: false,
			message: 'No token provided'
		});
	}
}
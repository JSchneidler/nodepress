var express = require('express');
var router = express.Router();
var path = require('path');

router.all('/*', function(req, res) {
	//res.send(path.resolve('public/master.html'));
	res.sendFile(path.resolve('public/master.html'));
});

module.exports = router;
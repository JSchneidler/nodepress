var express = require('express');
var app = express();
var router = express.Router();
var User = require('../server/models').User;
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var jwt_config = require('../config/jwt');
var jwt_middleware = require('./middleware/jwt');
var _ = require('lodash');

const salt_rounds = 10;
var tokenExpiresIn = "24h";

router.get('/setup', function(req, res) {
	var username = "jordan";
	var password = "pass";
	bcrypt.hash(password, salt_rounds, function(err, hash) {
		User.create({
			username: username,
			password: hash,
			admin: true,
			moderator: true
		}).then(function(user){
			return res.send(user);
		}).error(function(err) {
			console.log(err);
			return res.send(err);
		});
	});
});

router.get('/users', jwt_middleware, function(req, res) {
	User.findAll({ limit: 10 }).then(function(users) {
		return res.json({
			success: true,
			data: formatUsers(users)
		});
	}).error(function(err) {
		console.log(err);
		return res.json({
			success: false,
			message: "No users"
		});
	});
});

router.post('/login', function(req, res) {
	if (!req.body.username || !req.body.password) {
		return res.json({
			success: false,
			message: "Missing username and/or password"
		});
	} else {
		User.findOne({ where: { 'username': req.body.username } }).then(function(user) {
			if (user === null) {
				return res.json({
					success: false,
					message: "No user exists"
				});
			} else {
				bcrypt.compare(req.body.password, user.password, function(err, result) {
					if (err) {
						console.log(err);
						return res.json({
							success: false,
							message: "Server error"
						});
					} else if (result == true) {
						var angular_user = formatUser(user);
						var token = jwt.sign(angular_user, jwt_config.secret, {
							expiresIn: tokenExpiresIn
						});
						return res.json({
							success: true,
							token: token
						});
					} else {
						return res.json({
							success: false,
							message: "Incorrect password"
						});
					}
				});
			}
		}).error(function(err) {
			console.log(err);
			return res.json({
				success: false,
				message: "Server error"
			});
		});
	}
});

router.post('/register', function(req, res) {
	if (!req.body.username || !req.body.password) {
		return res.json({
			success: false,
			message: "Missing username and/or password"
		});
	} else {
		User.findOne({ where: { username: req.body.username } }).then(function(user) {
			if (user) {
				return res.json({
					success: false,
					message: "Username already exists"
				});
			}
			bcrypt.hash(req.body.password, salt_rounds, function(err, hash) {
				if (err) {
					console.log(err);
					return res.json({
						success: false,
						message: "Server error"
					});
				}
				User.create({
					username: req.body.username,
					password: hash
				}).then(function(user) {
					var angular_user = formatUser(user);
					var token = jwt.sign(angular_user, jwt_config.secret, {
						expiresIn: tokenExpiresIn
					});
					return res.json({
						success: true,
						token: token
					});
				}).error(function(err) {
					console.log(err);
					return res.json({
						success: false,
						message: "Server error"
					});
				});
			});
		}).error(function(err) {
			console.log(err);
			res.json({
				success: false,
				message: "Server error"
			});
		});
	}
});

function formatUser(user) {
	return _.omit(JSON.parse(JSON.stringify(user)), ['password', '__v']);
}

function formatUsers(users) {
	var formatted = [];
	for (var i = 0; i < users.length; i++) {
		formatted.push(formatUser(users[i]));
	}
	return formatted;
}

module.exports = router;
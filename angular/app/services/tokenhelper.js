angular.module('app').factory('TokenHelper', ['jwtHelper', function(jwtHelper) {

	function pullUserFromToken(token) {
		var payload = jwtHelper.decodeToken(token);
		return payload;
	}

	return {
		pullUserFromToken: pullUserFromToken
	};
}]);
app.directive('postArea', function() {
	return {
		restrict: 'A',
		scope: false,
		controller: PostCtrl
	};
});

var PostCtrl= ['$scope', 'Post', 'Error', function($scope, Post, Error) {
	$scope.posts = [];
  $scope.posts_loading = true;

  Post.query(function(res) {
      $scope.posts_loading = false;
      if (!res.success) {
          return Error.add('Server', 'Could not load posts');
      }
      $scope.posts = res.data;
      // Set time strings back to actual Date objects
      for (var i = 0; i < $scope.posts.length; i++) {
          $scope.posts[i].publish_date = new Date(+$scope.posts[i].publish_date);
      }
      console.log($scope.posts);
  });

  $scope.submitNewPost = function(title, body, user_id) {
      var post = {
          'title': title,
          'body': body,
          'user_id': user_id
      };
      Post.save(post, function(res) {
          if (!res.success) {
              return Error.add('Server', 'Could not save post');
          }
          $scope.posts.unshift(res.data);
      });
  }

  $scope.deletePost = function(post) {
      Post.delete({ id: post.id }, function(res) {
          if (!res.success) {
              return Error.add('Server', 'Could not delete post');
          }
          $scope.posts.splice($scope.posts.indexOf(post), 1);
      });
  }

  $scope.upvotePost = function(post) {
      Post.upvote({ id: post.id },function(res) {
          if (!res.success) {
              return Error.add('Server', 'Could not upvote post');
          }
          post.upvotes++;
      });
  }

  $scope.have_posts = function() {
      if ($scope.posts && $scope.posts.length > 0) {
          return true;
      }
      return false;
  }
}];
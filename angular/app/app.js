var app = angular.module("app", [
    "ngRoute",
    "ngResource",
    "angular-jwt",
    "ckeditor"
]);


app.config([
    "$routeProvider",
    "$locationProvider",
    "$httpProvider",
    "jwtInterceptorProvider",
    function($routeProvider,
        $locationProvider,
        $httpProvider,
        jwtInterceptorProvider) {
        //Route config
        $routeProvider
            .when("/", {
                templateUrl: "app/views/index.html",
                controller: "IndexCtrl"
            })
            .when("/login", {
                templateUrl: "app/views/login.html",
                controller: "AuthCtrl"
            })
            .when("/register", {
                templateUrl: "app/views/register.html",
                controller: "AuthCtrl"
            })
            .otherwise({
                templateUrl: "app/views/404.html"
            });

        // HTML5 mode
        $locationProvider.html5Mode(true);

        //JWT config
        jwtInterceptorProvider.tokenGetter = function() {
            if (storageAvailable('localStorage')) {
                return localStorage.getItem('jwt');
            }
            return;
        };

        $httpProvider.interceptors.push('jwtInterceptor');
    }
]);
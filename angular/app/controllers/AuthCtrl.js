angular.module('app').controller('AuthCtrl', ['$scope', '$rootScope', '$http', '$location', 'TokenHelper', "Error", function($scope, $rootScope, $http, $location, TokenHelper, Error) {

  $scope.loginOrRegister = function(username, password, type) {
    var error_type = type.charAt(0).toUpperCase() + type.slice(1);
    if (storageAvailable('localStorage')) {
      $http.post('/api/'+type, {
      'username': username,
      'password': password
      }).then(function(res) {
        if (!res.data.success) {
          Error.add(error_type, res.data.message);
        } else {
          localStorage.setItem('jwt', res.data.token);
          localStorage.setItem('user', JSON.stringify(res.data.user));
          $rootScope.user = TokenHelper.pullUserFromToken(res.data.token);
          $location.path('/');
        }
      }, function(err) {
        Error.add(error_type, err);
      });
    } else {
      Error.add("Client", "Local storage not available! You will not be able to login.");
    }
  }

}]);
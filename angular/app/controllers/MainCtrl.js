angular.module('app').controller('MainCtrl', ['$scope', '$rootScope', '$sce', '$location', 'TokenHelper', 'Error', function($scope, $rootScope, $sce, $location, TokenHelper, Error) {

	// Trust binded HTML
	$scope.trust = $sce.trustAsHtml;

	if (storageAvailable('localStorage')) {
		var token = localStorage.getItem('jwt');
		if (token) {
			$rootScope.user = TokenHelper.pullUserFromToken(token);
		}
	} else {
		Error.add("Client", "Local storage not available! You will not be able to login.");
	}

	$scope.isUser = function() {
		if ($rootScope.user) {
			return true;
		}
		return false;
	}

	$scope.userIsModerator = function() {
		if ($rootScope.user && $rootScope.user.moderator === true) {
			return true;
		}
		return false;
	}

	$scope.userIsAdmin = function() {
		if ($rootScope.user && $rootScope.user.admin === true) {
			return true;
		}
		return false;
	}

	$scope.logout = function() {
		localStorage.removeItem('jwt');
		$rootScope.user = null;
		$location.path('/');
	}

	$scope.haveErrors = function() {
		if (Error.getAll().length > 0) {
			return true;
		}
		return false;
	}

	$scope.removeError = function(error) {
		Error.remove(error);
	}

	$scope.clearErrors = function() {
		Error.clear();
	}

	// CKEditor config
	$scope.ck_options = {
		language: 'en',
		allowedContent: true,
		entities: true,
		tabSpaces: 4,
		autoParagraph: false
	};

}]);
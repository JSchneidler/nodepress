var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
	username: String,
	password: String,
	moderator: { type: Boolean, default: false },
	admin: { type: Boolean, default: false }
});

module.exports = mongoose.model('User', UserSchema);
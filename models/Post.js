var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
	title: String,
	body: String,
	upvotes: {type: Number, default: 0},
	comments: [{ body: String, date: { type: String, default: Date.now().toString() } }],
	date_published: { type: String, default: Date.now().toString() },
	date_updated: { type: String, default: Date.now().toString() }
});

/*
PostSchema.pre('update', function() {
	this.update({}, { $set: { date_updated: new Date() } });
});
*/

module.exports = mongoose.model('Post', PostSchema);
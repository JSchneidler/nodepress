module.exports = function(grunt) {
    require("time-grunt")(grunt);

    //grunt wrapper function
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        //grunt task config

        clean: {
            public: {
                files: [{
                    expand: true,
                    cwd: "public",
                    src: [
                        "**/*"
                    ]
                }]
            },
            views: {
                files: [{
                    expand: true,
                    cwd: "views",
                    src: [
                        "master.html"
                    ]
                }]
            }
        },

        copy: {
            angularAll: {
                files: [{
                    expand: true,
                    cwd: "angular",
                    dest: "public",
                    src: ["**/*"]
                }]
            },
            /*
            angularJustIndex: {
                files: [{
                    expand: true,
                    cwd: "angular",
                    dest: "public",
                    src: "master.html"
                }]
            },
            */
            unminifiedAngular: {
                files: [{
                    expand: true,
                    cwd: "angular",
                    dest: "public",
                    src: [
                        "images/**/*",
                        "views/**/*",
                        "fonts/**/*",
                        "favicon.ico",
                        "master.html"
                    ]
                }]
            },
            angularMaster: {
                files: [{
                    expand: true,
                    cwd: 'public',
                    src: 'master.html',
                    dest: 'views'
                }]
            },
            bowerComponents: {
                files: [{
                    expand: true,
                    cwd: "bower_components",
                    src: "**/*",
                    dest: "public/components"
                }]
            },
        },

        useminPrepare: {
            html: "angular/master.html",
            options: {
                dest: "public",
                flow: {
                    html: {
                        steps: {
                            js: ["uglifyjs"],
                            css: ["cssmin"]
                        }
                    }
                }
            }
        },

        uglify: { // usemin-created task
            options: {},
            generated: {}
        },

        cssmin: { //usemin-created task
            options: {},
            generated: {}
        },

        usemin: {
            html: "public/master.html",
            options: {
                assetsDirs: "public"
            }
        },

        nodemon: {
            dev: {
                script: "bin/www"
            }
        }

    });

    //Load grunt tasks
    grunt.loadNpmTasks("grunt-usemin");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-nodemon");

    //Register grunt build task (for dev)
    grunt.registerTask("build", [
        "clean:public",
        "clean:views",
        "copy:angularAll",
        "copy:angularMaster",
        "copy:bowerComponents"
    ]);

    //Register grunt build task and run nodemon (for dev)
    grunt.registerTask("buildrun", [
        "clean:public",
        "clean:views",
        "copy:angularAll",
        "copy:angularMaster",
        "copy:bowerComponents",
        "nodemon"
    ]);

    //Register grunt build minification task (for prod)
    grunt.registerTask("buildmin", [
        "clean:public",
        "clean:views",
        "copy:unminifiedAngular",
        "useminPrepare",
        "uglify:generated",
        "cssmin:generated",
        "usemin",
        "copy:angularMaster",
    ]);

    //Cleans public and resources/views folder
    grunt.registerTask("cleanD", [
        "clean:public",
        "clean:views"
    ]);
}